package com.demo.model;

public class Book {
	
	private String bookId;
	private String title;
	private String pub;
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Book(String bookId, String title, String pub) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.pub = pub;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPub() {
		return pub;
	}
	public void setPub(String pub) {
		this.pub = pub;
	}
	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", title=" + title + ", pub=" + pub + "]";
	}
	
	

}
