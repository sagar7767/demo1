package com.demo.test;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.demo.model.Book;
import com.demo.service.BookService;

class DemoAssertTrue {

	@Test
	public void assertTrueMsg() {
		BookService bookService = new BookService();
		Book javaBook = new Book("1","Java Complete reference", "Wrox");
		bookService.addBook(javaBook);
		List<Book> listOfBooks = bookService.books();
		assertTrue(listOfBooks.isEmpty(),"List of Book is not empty");
	}
	@Test
	public void assertFalseMsg() {
		BookService bookService = new BookService();
		Book javaBook = new Book("1","Java Complete reference", "Wrox");
		bookService.addBook(javaBook);
		List<Book> listOfBooks = bookService.books();
		assertFalse("List of Book is empty",listOfBooks.isEmpty());
	}
}
