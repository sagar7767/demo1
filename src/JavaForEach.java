import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class JavaForEach {

	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		for (int i = 0; i < 5; i++) {
			myList.add(i);
		}

		// Traditional Approch
		Iterator<Integer> itr = myList.iterator();
		while (itr.hasNext()) {
			Integer i = itr.next();
			System.out.println("i = " + i);
		}

		// Using ForEach
		myList.forEach(new Consumer<Integer>() {
			public void accept(Integer t) {
				System.out.println("For Each class value = " + t);
			}
		});

		MyConsumer action = new MyConsumer();
		myList.forEach(action);

	}

}

class MyConsumer implements Consumer<Integer> {

	@Override
	public void accept(Integer t) {
		System.out.println("Consumer impl value = "+ t);

	}

}