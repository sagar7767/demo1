import java.time.LocalDate;
import java.time.Month;

public class DemoDate {

	public static void main(String[] args) {
		LocalDate today = LocalDate.now();
		System.out.println("Current date = " + today);
		
		LocalDate firstDay = LocalDate.of(2018, Month.FEBRUARY, 28);
		System.out.println("Specific Date = " + firstDay);
		
		LocalDate todayKol = LocalDate.now();
	}

}
