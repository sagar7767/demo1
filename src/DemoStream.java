import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class DemoStream {

	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		for(int i =0; i<50; i++) {
			myList.add(i);
		}
		
		//Sequential stream
		Stream<Integer> seqStream = myList.stream();
		//Paraller Stream
		Stream<Integer> parStream = myList.parallelStream();
		
		Stream<Integer> highNums = parStream.filter(p-> p > 40);
		highNums.forEach(p-> System.out.println("High nums parallel = " + p));
		
		Stream<Integer> highNumSeq = seqStream.filter(p-> p > 40);
		highNumSeq.forEach(p-> System.out.println("High nums sequential = " + p));
		
		

	}

}
