import java.util.Optional;

public class DemoOption {

	public static void main(String[] args) {
		String[] str = new String[10];
		
		//After adding value
		str[5] = "Welcome to Java8";
		
		Optional<String> checkNull = Optional.ofNullable(str[5]);
		if(checkNull.isPresent()) {
			String lowCase = str[5].toLowerCase();
			System.out.println(lowCase);
		}else {
			System.out.println("String value not Present");
		}

	}

}
