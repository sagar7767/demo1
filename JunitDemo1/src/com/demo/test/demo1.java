package com.demo.test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

class demo1 {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.out.println("Before All Executed");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("After All Executed");
	}

	@BeforeEach
	void setUp() throws Exception {
		System.out.println("Before Each Executed");
	}

	@AfterEach
	void tearDown() throws Exception {
		System.out.println("After Each Executed");
	}

	@Disabled
	@Test
	void test() {
		fail("Not yet implemented");
	}
	@Tag("Dev")
	@Test
	public void TestAddOne() {
		System.out.println("-----Test One Executed-----");
		Assertions.assertEquals(4, Calculator.add(2, 2));
	}
	@Tag("Prod")
	@Test
	public void TestAddTwo() {
		System.out.println("-----Test Two Executed-----");
		Assertions.assertEquals(6, Calculator.add(2, 4));
	}
	
	@Test
	public void TestAddThree() {
		System.out.println("-----Test Three Executed-----");
		Assertions.assertNotEquals(7, Calculator.add(2, 4));
	}

}
